#ifndef CORE_H
#define CORE_H

// C++ API
void hello_cpp();

// C-compatible API
#ifdef __cplusplus
extern "C" {
#endif
    void hello_c();
#ifdef __cplusplus
}
#endif

#endif

